# learning-route

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Config

```
npm install vue-router@3.2.0
```
- In main.js: change to: import `Vue from 'vue/dist/vue.js';`