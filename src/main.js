// import Vue from 'vue'
import Vue from 'vue/dist/vue.js';
import App from './App.vue'
import VueRouter from 'vue-router'

// const Foo = { template: '<div>foo</div>' }
// const Bar = { template: '<div>bar</div>' }

const User = {
  template: '<div>  Hello user {{ $route.params.username }}, post id = {{ $route.params.post_id }}; params = {{ $route.params }}, {{$route.query}}</div>',
  watch: {
    // $route(to, from){
    //   if (to.params.username=="Tuong"){
    //     alert("User change from Tuong to Trung");
    //   }
    //   else if(to.params.username=="Trung"){
    //     alert("User change from Trung to Tuong");
    //   }
    //   console.log("to = ", to);
    //   console.log("from = ", from)
    // }
  },
  beforeRouteEnter (to, from, next) {
    console.log(to, from);
    next(vm => {
      // access to component instance via `vm`
      console.log("Value of vm = ", vm);
    })
  },
  // beforeRouteUpdate(to, from, next) {
  //   // react to route changes...
  //   // don't forget to call next()
    
  //   if (to.params.post_id == 5){
  //     console.log("In beforeRouteUpdate", from);
  //     this.$router.push('/user-AGGFFFF')
  //     console.log(this.$route.params.pathMatch) // Tra ve AGGFFFF
  //     next('/user/Trung/post/4')
  //   }
  //   next(20);
  // }
}
const DemoNewRoute = {
  template: '<div>{{$route.params}}</div>',
  watch: {
    $route(to, from) {
      if (to.params.name == "Jackson"){
        alert("Người chơi chuyển từ Napoleon sang Jackson");
      }
      else{
        alert("Other situation");
      }
      console.log(from, to);

    }
  }
}
const UserAsterisk = {
  template: '<div>  Hello UserAsterisk</div>',
}
const PageNotFound = {
  template: '<div>Page Not Found :((</div>'
}
// Map Link
const routes = [
  // { path: '/foo', component: Foo },
  // { path: '/bar', component: Bar },
  { path: '/user/:username/post/:post_id', component: User },
  { path: '/user-*', component: UserAsterisk},
  { path: '/demo-new-route/:name', component: DemoNewRoute},
  { path: '/*', component: PageNotFound}
]
const router = new VueRouter({
  routes // short for `routes: routes`
})

Vue.use(VueRouter)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
